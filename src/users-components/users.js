//<script src="./users.js"></script>

function mapFromAPIUser(apiUser) {
  let user= {
    "id": apiUser.id,
    "email": apiUser.email,
    "nombre": apiUser.firstName,
    "apellido": apiUser.lastName,
    "password": ""
  }
  return user;
}


function mapToAPIUser(user) {

  let id = user.id != undefined ? user.id : 1;
  let apiUser={
    "id": user.id,
    "email": user.email,
    "firstName": user.nombre,
    "lastName": user.apellido,
    "password": user.password
  }
  return apiUser;
}

/*
function mapFromAPIUser(apiUser) {
  let user= new UserClass();
  user.setValores(apiUser.email, api.firstName, api.lastName, "");

  return user;
}
*/
