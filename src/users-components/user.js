/**
 * Declaracion de la clase.
 */
class UserClass
{
    /**
     * Metodo principal para incializar nuestra clase.
     *
     * @return void.
     */
    constructor()
    {
        //Variables que estaran disponibles en el scope global
        //de la clase
        this.email = "";
        this.nombre = "";
        this.apellido = "";
        this.password = "";
        console.log("Se genero la instancia de CalculadoraUsandoClass");
    }//constructor


    /**
     * Metodo para definir los valores de la clase
     *
     * @param Integer valor_a Primer valor.
     * @param Integer valor_b Segundo valor.
     *
     * @return void.
     */
    setValores(email, nombre, apellido, password)
    {
        this.email = email;
        this.nombre = nombre;
        this.apellido = apellido;
        this.password = password;
    }

    /**
    * Getters y Setters
    */
    getEmail() {
      return this.email;
    }
    setEmail(email) {
      this.email = email;
    }

    getNombre() {
      return this.nombre;
    }
    setNombre(nombre) {
      this.nombre = nombre;
    }

    getApellido() {
      return this.apellido;
    }
    setApellido(apellido) {
      this.apellido = apellido;
    }

    getPassword() {
      return this.password;
    }
    setPassword(password) {
      this.password = password;
    }


}//CalculadoraUsandoClass
